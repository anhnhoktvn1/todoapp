Assignment 1:

    Project name : Todo-List (Ref : http://localtodos.com)
    Submit method: commit to bitbucket.org and inform Mentor via email
    Deadline: Before 9 PM - 18/8/2015
    Acceptance criteria
        Using only “pure” Javascript
        Support reorder elements in a list using the mouse like https://jqueryui.com/sortable/
        OOP is required
        Plus:
            Apply unit testing +
            Good UI/UX  ++
            Clean code  +++
            Functional automation testing +++++
