var UserPresentation = function(UserService){
	var self = {};
	self.logIn = function(username, password) {
		UserService.logIn({username: username, password: password}, function(err, user){
			if (err) {
				// error
				console.log('error', 'logIn', 'UserPresentation');
				window.alert('Wrong username or password! Please try again.');
			} else {
				window.location.href = 'index.html';
			}
		})
	};
	self.register = function(username, password) {
		UserService.addUser({username: username, password:password}, function(err, user){
			if (err) {
				// error 
				console.log('error', 'register', 'UserPresentation');
				window.alert('username existed! Please use another name.');
			} else {
				self.logIn(user.username, user.password);
			}
		});
	};
	return self;
}