var TaskPresentaion = function(Parse, TaskService){
	var self = {};

	var TODO_LIST = 'todo-list';
	var NEW_TASK_INPUT = 'new-todo';
	var CLEAR_COMPLETE_BUTTON = 'clear-completed';
	var LEFT_TASKS_TEXT = 'todo-count';
	var USERNAME_TEXT = 'username';
	var LOG_OUT_BUTTON = 'logOutButton';
	var TOGGLE_ALL_CHECKBOX = 'toggle-all';


	function onClearDoneTasks(){
		TaskService.clearDoneTasks(function(err){
			if (err){
				// error
				console.log('error', 'onClearDoneTasks', err);
			} else {
				loadTasks();
			}
		});
	}
	function onEnterEditingTask(e) {
		var element = e.currentTarget;
		element.className += " editing";
		document.getElementById('edt_' + element.id).focus();
	}
	function onLogOut(){
		Parse.User.logOut().then(function(){
			window.location.href = 'login.html';
		});
	}

	function onToggleTaskStatus(e){
		var element = e.currentTarget;
		var taskId = element.id.substr(4);
		toggleTaskStatus(taskId);
	}

	function onToggleTaskListStatus(e){
		var status = e.currentTarget.checked;
		toggleTaskListStatus(status);
	}


	function onRemoveTask(e){
		var element = e.currentTarget;
		var taskId = element.id.substr(4);
		removeTask(taskId);

	}
	function onCloseEdit(e) {
		var element = e.currentTarget;
		var taskId = element.id.substr(4);
		var taskElement = document.getElementById(taskId);
		var labelElement = taskElement.getElementsByTagName('label')[0];
		var description = element.value.trim();
		if (description == labelElement.innerHTML) {
			resetEditTask(taskElement);
			return false;
		}

		if (description == '') {
			removeTask(taskId);
			resetEditTask(taskElement);
			return false;
		}
		labelElement.innerHTML = description;
		resetEditTask(taskElement);
		TaskService.editTask(taskId, description, function(err, task){
			if (err){
		            // error
		            console.log(err);
		        } else {
		        	labelElement.innerHTML = task.get('description');
		        }
		    });

		return false;
	}
	function resetEditTask(taskElement){
		taskElement.className = taskElement.className.replace( /(?:^|\s)editing(?!\S)/g , '' );
	}
	function onEditTask(e) {
		if (!e) e = window.event;
		var keyCode = e.keyCode || e.which;

		if (keyCode == '13')
		{
			onCloseEdit(e);
		}
	}
	function onAddNewTask(e) {
		if (!e) e = window.event;
		var keyCode = e.keyCode || e.which;
		if (keyCode == '13')
		{
			var description = document.getElementById(NEW_TASK_INPUT).value.trim();
			if (description == "") {
				return false;
			}
			document.getElementById(NEW_TASK_INPUT).value = '';
			TaskService.addTask({description: description}, function(err, task){
				if (err){
	            // error
	            console.log(err);
	        } else {
	        	addTasks([task]);
	        }
	    });
			return false;
		}
	}

	function toggleTaskListStatus(status) {
		TaskService.setAllTasksStatus(status, function(err) {
			if (err) {
				// error
				console.log(err);
			} else {
				loadTasks();
			}
		});
	}
	function updateTaskListStatus() {
		var status = getTaskListInfo(TODO_LIST);
		console.log(status);
		var doneElement = document.getElementById(CLEAR_COMPLETE_BUTTON);
		var leftElement = document.getElementById(LEFT_TASKS_TEXT);
		var toggleAllElement = document.getElementById(TOGGLE_ALL_CHECKBOX);
		doneElement.innerHTML = 'Clear ' + status.done + ' completed item' + (status.done > 1 ? 's' : '');
		leftElement.innerHTML = '<b>' + status.left + '</b> item' + (status.left > 1 ? 's' : '') + ' left';
		doneElement.style.display = status.done > 0 ? 'block' : 'none';
		toggleAllElement.checked = status.done > 0 && status.left == 0 ;
	}

	function getTaskListInfo(taskListId) {
		var taskList = document.getElementById(taskListId);
		var tasks = taskList.getElementsByTagName('li');
		var total = tasks.length;
		var done = 0;
		for (var i = 0; i < total; i++) {
			task = tasks[i];
			done += task.className == "done" ? 1 : 0;
		}
		return {left: total - done, done:done};
	}

	function loadTasks(){
		TaskService.getTasks(function(err, tasks) {
			if (err) {
	        // error 
	    } else {
	    	document.getElementById(TODO_LIST).innerHTML = '';
	    	addTasks(tasks);
	    }
	});
	}

	function removeTask(taskId) {
		TaskService.removeTask(taskId, function(err){
			if (err) {
	        // error
	        console.log('error', 'removeTask', err);
	    } else {
	    	var taskElement = document.getElementById(taskId);
	    	taskElement.parentNode.removeChild(taskElement);
	    	updateTaskListStatus();
	    }
	});
	}
	function toggleTaskStatus(taskId) {
		TaskService.toggleTaskStatus(taskId, function(err, task){
			if (err) {
	        // error
	        console.log('error', 'toggleTaskStatus', err);
	    } else {
	    	updateTaskElementStatus(taskId, task.get('isDone'));
	    	updateTaskListStatus();
	    }
	});
	}

	function addTasks(tasks){
		var taskList = document.getElementById(TODO_LIST);
		var taskElement;

		tasks.forEach(function(task){
			taskElement = document.createElement('li');
			taskElement.id = task.id;
			taskElement.draggable = true;
			taskElement.innerHTML = '<div class="view"><input class="toggle" type="checkbox" id="chk_'
			+ task.id
			+'"><label>' 
			+ task.get('description') 
			+ '</label>        <a class="destroy" id="rmv_'
			+ task.id
			+'"></a>      </div>      <input class="edit" value="' 
			+ task.get('description') 
			+ '" type="text" id="edt_'
			+ task.id
			+'">    ';
			taskList.appendChild(taskElement);
			var checkTaskElement = document.getElementById('chk_' + task.id);
			checkTaskElement.onclick = onToggleTaskStatus;
			var removeTaskElement = document.getElementById('rmv_' + task.id);
			removeTaskElement.onclick = onRemoveTask;
			var editTaskElement = document.getElementById('edt_' + task.id);
			editTaskElement.onkeypress = onEditTask;
			editTaskElement.onblur = onCloseEdit;
			taskElement.ondblclick = onEnterEditingTask;
			updateTaskElementStatus(task.id, task.get('isDone'));
		});
		updateTaskListStatus();
	}

	function updateTaskElementStatus(taskId, isDone) {
		var taskElement = document.getElementById(taskId);
		taskElement.className = isDone ? "done" : "";
		var checkTaskElement = document.getElementById('chk_' + taskId);
		checkTaskElement.checked = isDone;
	}

	self.init = function() {
		Parse.initialize("viBknTboYGoQRZii7nHKfmGctcDBcYsQf1gCAq84", "weivFDNQcILNrhcPCa1bZMf0aut0k1WWUXEqfKiQ");
		var currentUser = Parse.User.current();
		if (currentUser) {
			document.getElementById(USERNAME_TEXT).innerHTML = 'Todos - ' + currentUser.get('username');
		} else {
			window.location.href = 'login.html';
			return;
		}

  		// load tasks
  		loadTasks();
  		document.getElementById(NEW_TASK_INPUT).onkeypress = onAddNewTask;
  		document.getElementById(LOG_OUT_BUTTON).onclick = onLogOut;
  		document.getElementById(CLEAR_COMPLETE_BUTTON).onclick = onClearDoneTasks;
  		document.getElementById(TOGGLE_ALL_CHECKBOX).onclick = onToggleTaskListStatus;

  	}

  	return self;
  }