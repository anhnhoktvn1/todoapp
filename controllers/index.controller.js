var indexController = function(_userService){
	var userService = _userService;
	var self = {};
	self.logIn = function(username, password) {
		userService.logIn({username: username, password: password}, function(err, user){
			if (err) {
				// error
				console.log('error', 'login', 'indexController');
				window.alert('Wrong username or password! Please try again.');
			} else {
				window.location.href = 'index.html';
			}
		})
	};
	self.register = function(username, password) {
		userService.addUser({username: username, password:password}, function(err, user){
			if (err) {
				// error 
				console.log('error', 'register', 'indexController');
				window.alert('username existed! Please use another name.');
			} else {
				self.logIn(user.username, user.password);
			}
		});
	};
	return self;
}