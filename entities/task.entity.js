var TaskFactory = function(Parse) {
	var Task = function(description, isDone, createdBy){
		var self = Parse.Object.extend("Task");
		self.set('description', description);
		self.set('isDone', isDone);
		var taskACL = new Parse.ACL(createdBy);
		taskACL.setPublicReadAccess(true);
		self.setACL(taskACL);

		return self;
	};


	return {
		CreateTask: Task;
	};
};
