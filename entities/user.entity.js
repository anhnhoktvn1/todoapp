var UserFactory = function(Parse) {
	var User = function(username, password){
		var self = Parse.Object.extend("User");
		self.set('username', username);
		self.set('password', password);

		return self;
	};


	return {
		CreateUser: User;
	};
};
