var TaskOrderFactory = function(Parse) {
	var TaskOrder = function(description, isDone, createdBy){
		var self = Parse.Object.extend("TaskOrder");
		self.set('description', description);
		self.set('isDone', isDone);
		var TaskOrderACL = new Parse.ACL(createdBy);
		TaskOrderACL.setPublicReadAccess(true);
		self.setACL(TaskOrderACL);

		return self;
	};


	return {
		CreateTaskOrder: TaskOrder;
	};
};
