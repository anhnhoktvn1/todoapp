var TaskDao = function(Parse, Task){
	var self = {};

	self.getTasks = function(next) {
		var query = new Parse.Query(Task);
		query.find()
		.then(function(tasks){
			next(null, tasks);
		}, function(err){
			next(err, null);
		});
	};
	self.getTask = function(taskId, next) {
		var query = new Parse.Query(Task);
		query.get(taskId)
		.then(function(task){
			next(null, task);
		}, function(err){
			next(err, null);
		});
	};
	self.getTasksByDescription = function(keyword, next) {
		var query = new Parse.Query(Task);
		query.equalTo("description", keyword); 
		query.find()
		.then(function(tasks){
			next(null, tasks);
		}, function(err){
			next(err, null);
		});
	};

	self.addTask = function(newTask, next) {
		var task = new Task();
		task.set("description", newTask.description);
		task.set("isDone", !!newTask.isDone);
		task.setACL(new Parse.ACL(Parse.User.current()));
		task.save(null)
		.then(function(task){
			next(null, task);
		}, function(err){
			next(err, null);
		});
	};

	self.editTask = function(taskId, description, next) {
		var query = new Parse.Query(Task);
		query.get(taskId)
		.then(function(task){
			task.save("description", description)
				.then(function(task){
					next(null, task);
				}, function(err){
					next(err, null);
				});
		}, function(err){
			next(err, null);
		});
	};
	self.setStatus = function(taskId, isDone, next) {
		var query = new Parse.Query(Task);
		query.get(taskId)
		.then(function(task){
			task.save("isDone", isDone)
				.then(function(task){
					next(null, task);
				}, function(err){
					next(err, null);
				});
		}, function(err){
			next(err, null);
		});
	};

	self.removeTask = function(taskId, next) {
		var query = new Parse.Query(Task);
		query.get(taskId)
		.then(function(task){
			task.destroy()
				.then(function(task){
					next(null, task);
				}, function(err){
					console.log('removeTask failed');
					next(err, null);
				});
		});
	};

	self.toggleTaskStatus = function(taskId, next) {
		var query = new Parse.Query(Task);
		query.get(taskId)
		.then(function(task){
			task.save("isDone", !task.get('isDone'))
				.then(function(task){
					next(null, task);
				}, function(err){
					console.log('toggleTaskStatus failed');
					next(err, null);
				});
		});
	};

	self.clearDoneTasks = function(next) {
		var query = new Parse.Query(Task);
		query.equalTo("isDone", true);
		query.find()
		.then(function(tasks){
			Parse.Object.destroyAll(tasks)
				.then(function(){
					next(null);
				}, function(err){
					console.log('clearDoneTasks failed');
					console.log(err);
					next(err);
				});
		});
	};

	self.setAllTasksStatus = function(status, next) {
		var query = new Parse.Query(Task);
		query.find()
		.then(function(tasks){
			tasks.forEach(function(task){
				task.set('isDone', status);
			});
			Parse.Object.saveAll(tasks)
				.then(function(){
					next(null);
				}, function(err){
					console.log('checkDoneAllTasks failed');
					console.log(err);
					next(err);
				});
		});
	};

	self.removeAllTasks = function(next) {
		var query = new Parse.Query(Task);
		query.find()
		.then(function(tasks){
			Parse.Object.destroyAll(tasks)
				.then(function(){
					next(null);
				}, function(err){
					console.log('removeAllTasks failed');
					console.log(err);
					next(err, null);
				});
		});
	};

	return self;
};
