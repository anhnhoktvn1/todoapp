var userDao = function(Parse) {
	var self = {};

	self.getUsers = function(username, next) {
		var query = new Parse.Query(Parse.User);
		query.equalTo("username", username);
		query.find()
			.then(function(users) {
				next(null, users);
			}, function(err) {
				next(err, null);
			});
	};

	self.addUser = function(newUser, next) {
		var user = new Parse.User();
		user.set("username", newUser.username);
		user.set("password", newUser.password);
		user.signUp(null)
			.then(function(user) {
				next(null, user);
			}, function(err) {
				console.log('addUser failed');
				console.log(err);
				next(err, null);
			});
	};

	self.removeUser = function(username, next) {
		var query = new Parse.Query(Parse.User);
		query.equalTo("username", username);
		query.find()
			.then(function(users) {
				var user = users[0];
				user.destroy()
					.then(function(user) {
						next(null, user);
					}, function(err) {
						console.log('removeUser failed');
						console.log(err);
						next(err, null);
					});
			});
	};


	self.logIn = function(user, next) {
		Parse.User.logIn(user.username, user.password)
			.then(function(user) {
				next(null, user);
			}, function(err) {
				console.log('logIn failed');
				console.log(err);
				next(err, null);
			});

	};
	self.logOut = function(next) {
		Parse.User.logOut()
			.then(function() {
				next(null);
			}, function(err) {
				console.log('logOut failed');
				console.log(err);
				next(err);
			});

	};

	return self;
};